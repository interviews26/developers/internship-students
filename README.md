# The Task for Internship Students Focused on Development

The task is to create a simple textual-based game. In this game the players object will be created. The player will go through the game world until he met the final destination (the game story is up to you).

**Requirements for a player**:
- The player will have fields as "name" and "nickname".

**Requirements for an application**:
- After starting the application, the application will ask you for the "name" and "nickname", which you enter in the command line (this will create your player object).
When the player object is created, the application will ask you what location you want to go to. The application will give you options (e.g., A-D) from that you can select. Based on the selected option, the player will move to the next location.
- The game must contain at least 15 locations. Some edges between locations must be bidirectional.
- The game has not to be only linear, there must be more options to go through the game.
- Draw the structure of your game world and attach it to the resulting repository.

**Hint**: Implement the application as a graph structure. The game will save the actual location of the player (at what node the player is). Every node will contain the list of next nodes representing the connections (or you can implement it via nodes, edges, etc. structures).

**The example game**:
The example structure of a game: "Finding a home from a pub." (do not implement this example, design and implement your own).
<div style="text-align:center">
    <img src="images/graph-structure.PNG" width="450">
</div>
When the player starts the game it ask the player for his "name" and "nickname". Further, it gives the player two options:

* [A] Go to the "Pub 1".
* [B] Go to the "Pub 8".

player select one of this options, and moves to the "Pub 1" or "Pub 8". Then the program will ask for another choice and so on. When the player moves to the "End" the player wins.

**Optional extensions**: 
- Consider how you would implement if data such as path length were to be added to the game, so each edge would have to hold this information.
- Implement objects that will interact with the player (in the case of my game it will be bar tender/pub, ...) so that the teammate and the object "waiting" in the given location communicate in some way (e.g., request for a beer ticket, delivery beers/replenishment of lives).
-  Implement the possibility to save the current state of the game. Further, store the player with his score (define the scoring mechanism). Provide the scoreboard for the players. Store all of these into the database or suitable data structure (JSON/XML).

